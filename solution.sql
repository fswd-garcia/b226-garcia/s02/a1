-- s02: Translating Database Design
-- 1. Given the following ERD, create a new s02-a1 folder and then create a file named solution.sql and write the SQL code necessary to create the shown tables and images.
-- 2. Create a blog_db database.
-- Note: Also include the primary key and foreign key constraints in your answer.

CREATE DATABASE blog__db;

CREATE TABLE users(
	id INT NOT NULL,
	email VARCHAR(100),
	password VARCHAR(300),
	datetime_created DATETIME NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE posts(
	id INT NOT NULL,
	author_id INT NOT NULL,
	title VARCHAR(500),
	content VARCHAR(5000),
	datetime_posted DATETIME NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_posts_author_id 
		FOREIGN KEY (author_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

CREATE TABLE post_comments(
	id INT NOT NULL,
	content VARCHAR(5000),
	datetime_commented DATETIME NOT NULL,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_comments_post_id 
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_post_comments_user_id 
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);



CREATE TABLE post_likes(
	id INT NOT NULL,
	datetime_liked DATETIME NOT NULL,
	post_id INT NOT NULL,
	user_id INT NOT NULL,
	PRIMARY KEY (id),
	CONSTRAINT fk_post_likes_post_id 
		FOREIGN KEY (post_id) REFERENCES posts(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT,
	CONSTRAINT fk_post_likes_user_id 
		FOREIGN KEY (user_id) REFERENCES users(id)
		ON UPDATE CASCADE
		ON DELETE RESTRICT
);

